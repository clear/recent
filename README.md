# recent

A program to print the most recently modified/created file/directory in a given directory. Inspired by the Unix philosophy.

recent - list most recent files.

### Update: 
Sorting is now in, allowing multi-line output. STDIN is also supported now. 

This program is written in Go, and is meant to fit into the Unix ecosystem as a nifty little tool. 
For many years, I always used `$(ls -art1 | tail -n 1)` in my pipes and scripts. However, I wanted a tool that worked simply, in true Unix fashion, to just print out the most recent file, with multiline inputs and outputs.
This program tries to be that tool. I hope it can be useful to you.
Pull requests encouraged.

## Install Instructions
### Pre-Requisites
- A working Go installation
- correct PATH

`git clone https://codeberg.org/clear/recent.git`
`cd recent; go install .`
Now just run `recent`!

## To-Do
- Add flag parsing [done]
- Add listing by creation time
- Add actual build method (for different versions for *nix/Windows)
- Optimize
