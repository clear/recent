package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"syscall"
)

var (
	debug   bool = false
	dir     string
	lsdir   bool = false
	created bool
	lines   int
	result  []FileObject
)

const usage = `Usage of recent:
  -c, --created
    	Sort by creation date instead by modified date
  -n, --lines int
    	Lines of output, descending by most recent (default 1)
  -x, --no-dir
    	Do not include directories (included by default)
  --debug
    	Debug information
`

type FileObject struct {
	Name  string
	Time  int64
	IsDir bool
}

func printa(a []FileObject) {
	for _, t := range a {
		fmt.Println(t.Name)
	}
}

func printr(a []FileObject) {
	i := 0
	for j := len(a) - 1; j > 0; j-- {
		if i == lines {
			break
		}
		fmt.Println(a[j].Name)
		i++
	}
}

func dirname(a string) string {
	if (string(a[len(a)-1])) == "/" {
		return a
	} else {
		return fmt.Sprintf("%s/", a)
	}
}

func quickSort(arr []FileObject, low, high int64) []FileObject {
	if low < high {
		var p int64
		arr, p = partition(arr, low, high)
		arr = quickSort(arr, low, p-1)
		arr = quickSort(arr, p+1, high)
	}
	return arr
}

func quickSortStart(arr []FileObject) []FileObject {
	return quickSort(arr, int64(0), int64(len(arr)-1))
}

func partition(arr []FileObject, low, high int64) ([]FileObject, int64) {
	pivot := arr[high].Time
	i := low
	for j := low; j < high; j++ {
		if arr[j].Time < pivot {
			arr[i], arr[j] = arr[j], arr[i]

			i++
		}
	}
	arr[i], arr[high] = arr[high], arr[i]
	return arr, i
}

func byCreated() []FileObject { return []FileObject{} } //not done!

func byModified(dir string) []FileObject {
	//maybe make dir validation separate function?
	files, err := os.ReadDir(dir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: empty or non-existent directory\nDetailed error message: %s\n", err)
		os.Exit(1)
	}

	var (
		sec        int64
		newest     int64
		newestFile string
		currTime   int64
		isdirl     bool
	)

	n := make([]FileObject, len(files))

	for pos, f := range files {
		if debug == true {
			fmt.Printf("Iteration %d; pos length: %d; files length: %d\n", pos, len(n), len(files))
		}

		//gets file object
		fi, err := os.Lstat(dir + f.Name())
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err)
			os.Exit(1)
		}
		if !lsdir {
			if fi.IsDir() {
				continue
			}
		}
		//gets underlying syscall output
		sysStat, ok := fi.Sys().(*syscall.Stat_t)
		if !ok {
			fmt.Errorf("Error! The syscall to the operating system failed; Most likely, you're on Windows. This tool isn't really for that, at least not yet.")
			os.Exit(1)
		}

		if ok {
			//assigns accurate modifiedtimes
			currTime := sysStat.Mtim
			sec = currTime.Sec
		}
		//debug options
		if debug == true {
			fmt.Println(f.Name())
			fmt.Println(currTime)
			fmt.Println(sec)
			fmt.Println(fi.Sys())
			fmt.Println("")
		}

		if lines == 1 {
			if sec > newest {
				newest = sec
				isdirl = fi.IsDir()
				newestFile = f.Name()
			}
		} else {
			newf := FileObject{Name: f.Name(), Time: sec, IsDir: fi.IsDir()}
			//t[pos] = sec
			//n[pos] = f.Name()
			n[pos] = newf
		}
	}

	if lines == 1 {
		newestFO := FileObject{Name: newestFile, Time: newest, IsDir: isdirl}
		return []FileObject{newestFO}
	}
	return n
}

func main() {

	flag.BoolVar(&debug, "debug", false, "Debug information")
	flag.BoolVar(&created, "created", false, "Sort by creation date instead by modified date")
	flag.BoolVar(&created, "c", false, "Sort by creation date instead by modified date")
	flag.BoolVar(&lsdir, "no-dir", true, "Do not include directories (included by default)")
	flag.BoolVar(&lsdir, "x", true, "Do not include directories (included by default)")
	flag.IntVar(&lines, "lines", 1, "Lines of output, descending by most recent (default: 1)")
	flag.IntVar(&lines, "n", 1, "Lines of output, descending by most recent (default: 1)")
	flag.Usage = func() { fmt.Print(usage) }
	flag.Parse()

	stat, _ := os.Stdin.Stat()

	if (stat.Mode() & os.ModeCharDevice) == 0 {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			dir = dirname(scanner.Text())
			if created {
				result = byCreated()
				printa(result)
			} else {
				result = byModified(dir)
				printr(result)
			}
		}
		os.Exit(0)
	} else {
		//begin directory parsing
		if len(flag.Args()) == 0 {
			// if no directory supplied; use cwd
			curdir, err := os.Getwd()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			//add trailing slash for os.Stat; rev:NOT NEEDED
			dir = fmt.Sprintf("%s/", curdir)
		} else if len(flag.Args()) == 1 {
			tdir := flag.Arg(0)
			//if trailing slash supplied, don't add slash; rev:NOT NEEDED
			dir = dirname(tdir)
		} else {
			fmt.Println("Too many arguments")
			os.Exit(1)
		}

	}

	if created {
		result = byCreated()
		printa(result)
	} else {
		if lines == 1 {
			result = byModified(dir)
			printa(result)
		} else {
			result = byModified(dir)
			sorted := quickSortStart(result)
			printr(sorted)
		}
	}
	//TODO: add byCreated function and find creation date
}
